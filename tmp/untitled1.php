<?php

function Main_Cal($MainTable, $CalTable, $dbar){
		$Month = 0;
		$KM_Current = 1;
		$KM_Next = 0;
		$Step = 0;
		$Period = 0;
		$rr=0;
		$GG = 0;
		do{
			// echo $Month.'<br>';
			do{
				//Cal KM Current Month
				$KM_Current = Cal_KM($CalTable,$Month,$Month+$Step);
				$Step++;
				//Change Quantity on CurrentMonth and NextMonth 
				$CalTable[1][$Month] = $CalTable[1][$Month] + $CalTable[1][$Month+$Step];
				$CalTable[1][$Month+$Step] = 0;
				// //Re Cal CalTable
				$CalTable = Refresh($CalTable, $Month, $Month+$Step, $dbar);
				// echo "$Month, $Month+$Step".'<br>';
				
				// //Cal KM Average(CurrentMonth and NextMonth)
				$KM_Next = Cal_KM($CalTable, $Month, $Month+$Step);
				
				// //Check KM
				// echo $KM_Current . "  " . $KM_Next . '<br>';
				if($KM_Current > $KM_Next){
					$Period++;
				} 
				// else{
					// $KM_Next=0;
				// }
				
			// 	//Show Result
			// 	// if($Period<=1){
			// 	// 	System.out.println("Month : "+(Month+1)+"-"+(Month+Step+1));
			// 	// 	System.out.format("%12s:%12s%n", "Current("+(Month+1)+")",KM_Current);
			// 	// }
			// 	// System.out.format("%12s:%12s%n", "Next("+(Month+1)+"-"+(Month+Step+1)+")",KM_Next);

			}while(($KM_Current>$KM_Next) && (($Month+$Step)<11));
			// echo $KM_Current . "  " . $KM_Next . '<br>';
			if($Period > 0){
				//Fill MainTable with CalTable
				$CC = Copy_Table($CalTable,$MainTable,$Month,$Month+$Period);
				$MainTable = $CC["MainT"];
				$CalTable = $CC["CalT"];
			}else{
				//Fill NextMonth on CalTable with MainTable
				$CC = Copy_Table($MainTable,$CalTable,$Month,$Month+$Step);
				$MainTable = $CC["MainT"];
				$CalTable = $CC["CalT"];
				// echo $MainTable[1][2];
			}
			
			//Reset variable in loop
			$Period = 0;
			if ($Step == 0){
				$Month += 1;
				// $GG = $GG + (0 + $Step);
			} else {
				$Month += $Step;
			}
			// $Month+=$Step;
			// echo "$Month + $Step = $GG". '<br>';
			$Step = 0;
		    $rr++;
		    if($rr==12){
				exit();
			}
		}while($Month<11);
		// echo "$MainTable";
		$SM_DATA = ShowResult($MainTable);
		return $SM_DATA;
}
	
function Cal_KM($table, $start, $stop){
		$sum=0;
		for($i=$start; $i<=$stop; $i++){
			$sum = $sum + ($table[5][$i]+$table[6][$i])/($stop-$start+1);
		}
		// echo $sum;
		return $sum;
	}
	
function ShowResult($table){
	$Total = 0;
	$Total_HC = 0;
	$Total_OC = 0;
	// echo '<table style="width:100%">';
		for($i=0; $i<=6; $i++){
			// echo '<tr>';
			for($j=0; $j<=11; $j++){
				// echo '<td>'.$table[$i][$j].'</td>';
				if($i==5 || $i==6){
					$Total += $table[$i][$j];
				}
				if($i==5){
					$Total_HC += $table[$i][$j];
				}
				if($i==6){
					$Total_OC += $table[$i][$j];
				}
			}
			// echo '</tr>';
		}
		// echo "$Total_HC + $Total_OC = $Total";
	$data = array(2 => $Total_HC, 3 => $Total_OC, 1 => $Total);
	return $data;
	// echo '</table>';
	}
	
function Refresh($table, $start, $stop, $dbar){
	// global $dbar;
	// echo "$start, $stop".'<br>';
		for($i=$start; $i<=$stop; $i++){
			if($i==0){
				$table[2][$i] = $table[1][$i];
				$table[3][$i] = $table[2][$i] - $table[0][$i];
				$table[4][$i] = ($table[3][$i]+$table[2][$i])/2;
				$table[5][$i] = $table[4][$i]*($dbar[2]/12)*$dbar[3];
				if($table[1][$i]>0){
					$table[6][$i] = $dbar[1];
				}else{
					$table[6][$i] = 0;
				}
			}else{
				$table[2][$i] = $table[3][$i-1]+$table[1][$i];
				$table[3][$i] = $table[2][$i]-$table[0][$i];
				$table[4][$i] = ($table[3][$i]+$table[2][$i])/2;
				$table[5][$i] = $table[4][$i]*($dbar[2]/12)*$dbar[3];
				if($table[1][$i]>0){
					$table[6][$i] = $dbar[1];
				}else{
					$table[6][$i] = 0;
				}
				
			}
		}
		return $table;
	}
	
function Copy_Table($MainTable, $CalTable, $start, $n){
		for($i=0; $i<=6; $i++){
			for($j=$start; $j<=$n; $j++){
				$CalTable[$i][$j] = $MainTable[$i][$j];
				// echo "XXXXXX".$MainTable[$i][$j].'<br>';
				// echo $CalTable[$i][$j].'<br>'.$MainTable[$i][$j].'<br>';
			}
		}
	 $value = array("MainT" =>$MainTable, "CalT" => $CalTable );
	 return $value;
	}
	
function SM($product_name, $rawDATA){
		global $dbar;
		$CalTable[0][0] = "";
		$MainTable = InitialMainTable($rawDATA);
		$dbar = Initial_dbar($MainTable,$product_name);
		$MainTable = Refresh($MainTable,0,11,$dbar);
		$CC = Copy_Table($MainTable, $CalTable, 0, 11);
		$MainTable = $CC["MainT"];
		$CalTable = $CC["CalT"];
		// echo $CalTable[1][2];
		$data = Main_Cal($MainTable, $CalTable,$dbar);
		return $data;
	}
	
function Initial_dbar($MainTable,$product_name){
		//Cal dbar
		// global $dbar, $MainTable;
	include 'db/db_conn.php';
	$sql = "SELECT * FROM product";
	$resultx = $conn->query($sql);
	if ($resultx->num_rows > 0) {
	    // output data of each row
	    while($rowO=$resultx->fetch_assoc()) {
	    	if ($rowO["PRODUCT_NAME"] == $product_name){
	            $S = $rowO["S"];
	            $h = $rowO["H"];
	            $C = $rowO["C"];
	            $MIN = $rowO["MIN"];
	            // echo $S.$h.$C.'<br>';
	        }
	    }
	}
	 else {
	    echo "0 results";
	    exit;
	}
	    $dbarx[0] = 0;
		for($i=0;$i<12;$i++){
			$dbarx[0]+=$MainTable[0][$i];
		}
		$dbarx[0] = $dbarx[0]/12;
		$dbarx[1] = (148000.00/3/86);
		$dbarx[2] = 0.20;
		$dbarx[3] = $C;
		$dbarx[4] = round($dbarx[3]*(0.2/12),2);
		return $dbarx;
	}
	
function InitialMainTable($rawDATA){
		//Assign Demand
		// global $MainTable;
		for($i=0; $i<=1; $i++){
			for($j=0; $j<12; $j++){
				$MainTable[$i][$j] = $rawDATA[$j];
			}
		}
			// $MainTable[$i][1] = 1514;
			// $MainTable[$i][2] = 1357;
			// $MainTable[$i][3] = 1124;
			// $MainTable[$i][4] = 1266;
			// $MainTable[$i][5] = 1121;
			// $MainTable[$i][6] = 868;
			// $MainTable[$i][7] = 434;
			// $MainTable[$i][8] = 405;
			// $MainTable[$i][9] = 452;
			// $MainTable[$i][10] = 6;
			// $MainTable[$i][11] = 48;
		// }
		$MainTable[1][0] = $MainTable[0][0];
		return $MainTable;
	}
// function main() {
		// SMValues();
		// echo $MainTable[0][0].'<br>';
		// echo $dbar[4].'<br>';
		
	// }
?>