<!DOCTYPE html>
<html lang="en">

<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="shortcut icon" href="img/favicon.png" />
    <link rel="stylesheet" href="build/css-mint.min.css">
    <style>
    .grid *[class*='col-span'] > div {
        color: #fff;
        background-color: #5C9DED;
        margin: 5px;
        padding: 7px;
        font-weight: 300;
    }
    </style>
</head>

<body style="padding: 30px 30px 50px;">
    <header class="header">
        <div class="logo">
            <a href="index.php"><h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1></a>
        </div>
        <div class="nav-right">
            <nav class="navbar">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </nav>
        </div>
    </header><br><br>

    <?php
        error_reporting(0);
        session_start();
        include 'db/db_conn.php';
        include 'sm_cal.php';
        include 'function.php';
        if (isset($_POST['productNamevc']) && isset($_POST['vcCal']) && isset($_POST['productData'])){
            $product_name_vc = $_POST['productNamevc'];
            $vcCal = $_POST['vcCal'];
            $productData2 = unserialize($_POST['productData']);
            $highestRow = $_POST['highestRowPN'];
            $highestColumn = $_POST['highestColumnPN'];
            $_SESSION["SORIGIN"] = "0";
            $_SESSION["SEOQ"] = "0";
            $_SESSION["SPOQ"] = "0";
            $_SESSION["SSM"] = "0";
            $_SESSION["SWW"] = "0";
            if (isset($_POST['CEOQ'])){
                $_SESSION["SEOQ"] = $_POST['CEOQ'];
            }
            if (isset($_POST['CPOQ'])){
                $_SESSION["SPOQ"] = $_POST['CPOQ'];
            }
            if (isset($_POST['CSM'])){
                $_SESSION["SSM"] = $_POST['CSM'];
            }
            if (isset($_POST['CWW'])){
                $_SESSION["SWW"] = $_POST['CWW'];
            }
        } else {
            $product_name_vc = $_SESSION["product_name_vc"];
            $vcCal = $_SESSION["vcCal"];
            $productData2 = $_SESSION["productData2"];
            $highestRow = $_SESSION["highestRow_PN"];
            $highestColumnPN = $_SESSION["highestColumn_PN"];
        }
    ?>
    <br><br>
    <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a href="excel-upload.php">เลือกวิธีการสั่งซื้อ</a></li>
        <li><a href="#">เปรียบเทียบวิธีการสั่งซื้อ</a></li>
    </ul>
    <?php
        echo '<br><h4><font face="verdana">'."ชื่อสินค้า : $product_name_vc".'</font></h4>';

        echo '<center><table style="width:100%;" border=1 class="info">';

        $PRODUCT_COUNT = 1;
        $xx=1;
        $sum_data1 = 0;
        $sum_data2 = 0;

        $product_names = $product_name_vc;
        $rowData = $productData2;
        $MOUNT_COUNT = 1;
        for($i=0;$i<12;$i++){
                $sum_data1 = $sum_data1+$productData2[$i];
                $sum_data2 = $sum_data2+($productData2[$i]*$productData2[$i]);
        }
        $productData = $rowData;
        $dBar_x = dBar($sum_data1, 12);
        $estvard = est_var_d($sum_data1, $sum_data2, 12);
        $vcOriginal = vc($sum_data1, $sum_data2, 12);

        $TCS = original($product_names,$rowData);
        $_SESSION["ORIGINAL"] = $TCS;

        $simulSummarys=ww_cal($product_names,$rowData);
        $_SESSION["WW_DATA"]=[$product_names,$rowData];
        $wwHc=round($simulSummarys->calcHoldingCost(),2);
        $wwOc=round($simulSummarys->calcOrderingCost(),2);



        echo '<thead>
                <tr>
                    <th><center>วิธีการสั่งซื้อ</th>
                    <th><center>ค่าใช้จ่ายในการสั่งสินค้า (บาท)</th>
                    <th><center>ค่าใช้จ่ายในการถือครองสินค้า (บาท)</th>
                    <th><center>ต้นทุนการจัดการสินค้าคงคลังโดยรวม (บาท)</th>
                    <th><center>ส่วนต่าง (บาท)</th>
                    <th><center>ส่วนต่าง (%)</center></th>
                </tr>
             </thead>';

        $SUMORI = $TCS[1]-$TCS[1];
        $PERCENORI = $SUMORI/100;
        echo '<tr>';
        echo '<th><a href="show.php?name=วิธีปัจจุบัน">วิธีปัจจุบัน</th>
                <td align="center">'.number_format(round($TCS[10],2), 2, '.', ',').'</td>
                <td align="center">'.number_format(round($TCS[11],2), 2, '.', ',').'</td>
                <td align="center">'.number_format(round($TCS[1],2), 2, '.', ',').'</td>
                <th>'.number_format(round($SUMORI,2), 2, '.', ',').'</th>
                <th>'.number_format(round($PERCENORI,2), 2, '.', ',').'</th>';
        echo '</tr>';
        $PERCEN_EOQ = 0;
        $PERCEN_POQ = 0;
        $PERCEN_SM = 0;

        if ($_SESSION["SEOQ"] != "0"){
            $TEOQ = EOQ($product_names,$rowData,$dBar_x,$sum_data1);
            $SUM_EOQ = $TCS[1]-$TEOQ[1];
            $PERCEN_EOQ = ($SUM_EOQ/$TCS[1])*100 ;
            $_SESSION["EOQ"] = $TEOQ;
        }
        if ($_SESSION["SPOQ"] != "0"){
            $TPOQ = POQ($product_names,$rowData,$dBar_x,$sum_data1);
            $SUM_POQ = $TCS[1]-$TPOQ[1];
            $PERCEN_POQ = ($SUM_POQ/$TCS[1])*100 ;
            $_SESSION["POQ"] = $TPOQ;
        }
        if ($_SESSION["SSM"] != "0"){
            $TSM = SM($product_names, $rowData);
            $SUM_SM = $TCS[1]-$TSM[1];
            $PERCEN_SM = ($SUM_SM/$TCS[1])*100 ;
            $_SESSION["SM"] = $TSM;
        }
        if ($_SESSION["SWW"] != "0"){
            $TWW = $wwOc+$wwHc;
            $SUM_WW = $TCS[1]-$TWW;
            $PERCEN_WW = ($SUM_WW/$TCS[1])*100 ;
            $_SESSION["WW"] = $TWW;
        }

        if ($_SESSION["SEOQ"] != "0"){
            if (($PERCEN_EOQ >= $PERCENORI) && ($PERCEN_EOQ >= $PERCEN_POQ) && ($PERCEN_EOQ >= $PERCEN_SM)) {
                echo '<tr>';
                echo '<th><font color="green"><a href="show.php?name=EOQ">วิธี EOQ</a></th>
                        <th><font color="green">'.number_format(round($TEOQ[10],2), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($TEOQ[11],2), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($TEOQ[1],2), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($SUM_EOQ,2), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($PERCEN_EOQ,2), 2, '.', ',').'</font></th>';
                echo '</tr>';
            } else {
                echo '<tr>';
                echo '<th><a href="show.php?name=EOQ">วิธี EOQ</a></th>
                        <td align="center">'.number_format(round($TEOQ[10],2), 2, '.', ',').'</td>
                        <td align="center">'.number_format(round($TEOQ[11],2), 2, '.', ',').'</td>
                        <td align="center">'.number_format(round($TEOQ[1],2), 2, '.', ',').'</td>
                        <th>'.number_format(round($SUM_EOQ,2), 2, '.', ',').'</th>
                        <th>'.number_format(round($PERCEN_EOQ,2), 2, '.', ',').'</th>';
                echo '</tr>';
            }
        }
        if ($_SESSION["SPOQ"] != "0"){
            if (($PERCEN_POQ >= $PERCENORI) && ($PERCEN_POQ >= $PERCEN_EOQ) && ($PERCEN_POQ >= $PERCEN_SM)) {
                echo '<tr>';
                echo '<th><font color="green"><a href="show.php?name=POQ">วิธี POQ</a></th>
                        <th><font color="green">'.number_format(round($TPOQ[10],2), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($TPOQ[11],2), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($TPOQ[1],2), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($SUM_POQ,2), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($PERCEN_POQ,2), 2, '.', ',').'</font></th>';
                echo '</tr>';
            } else {
                echo '<tr>';
                echo '<th><a href="show.php?name=POQ">วิธี POQ</a></th>
                        <td align="center">'.number_format(round($TPOQ[10],2), 2, '.', ',').'</td>
                        <td align="center">'.number_format(round($TPOQ[11],2), 2, '.', ',').'</td>
                        <td align="center">'.number_format(round($TPOQ[1],2), 2, '.', ',').'</td>
                        <th>'.number_format(round($SUM_POQ,2), 2, '.', ',').'</th>
                        <th>'.number_format(round($PERCEN_POQ,2), 2, '.', ',').'</th>';
                echo '</tr>';
            }
        }
        if ($_SESSION["SSM"] != "0"){
            if (($PERCEN_SM >= $PERCENORI) && ($PERCEN_SM >= $PERCEN_POQ) && ($PERCEN_SM >= $PERCEN_EOQ)) {
                echo '<tr>';
                echo '<th><font color="green"><a href="show.php?name=SM">วิธี SM</a></th>
                        <th><font color="green">'.number_format(round($TSM[3],2), 2, '.', ',').'</td>
                        <th><font color="green">'.number_format(round($TSM[2],2), 2, '.', ',').'</td>
                        <th><font color="green">'.number_format(round($TSM[1],2), 2, '.', ',').'</td>
                        <th><font color="green">'.number_format(round($SUM_SM,2), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($PERCEN_SM,2), 2, '.', ',').'</font></th>';
                echo '</tr>';
            } else {
                echo '<tr>';
                echo '<th><a href="show.php?name=SM">วิธี SM</a></th>
                        <td align="center">'.number_format(round($TSM[3],2), 2, '.', ',').'</td>
                        <td align="center">'.number_format(round($TSM[2],2), 2, '.', ',').'</td>
                        <td align="center">'.number_format(round($TSM[1],2), 2, '.', ',').'</td>
                        <th>'.number_format(round($SUM_SM,2), 2, '.', ',').'</th>
                        <th>'.number_format(round($PERCEN_SM,2), 2, '.', ',').'</th>';
                echo '</tr>';
            }
        }
        if ($_SESSION["SWW"] != "0"){
            if (($PERCEN_WW >= $PERCENORI) && ($PERCEN_WW >= $PERCEN_POQ) && ($PERCEN_WW >= $PERCEN_EOQ)&& ($PERCEN_WW >= $PERCEN_SM)) {
                echo '<tr>';
                echo '<th><font color="green"><a href="ww/index.php?name=WW">วิธี WW</a></th>
                        <th><font color="green">'.number_format(round($wwOc), 2, '.', ',').'</td>
                        <th><font color="green">'.number_format(round($wwHc), 2, '.', ',').'</td>
                        <th><font color="green">'.number_format(round($TWW), 2, '.', ',').'</td>
                        <th><font color="green">'.number_format(round($SUM_WW), 2, '.', ',').'</th>
                        <th><font color="green">'.number_format(round($PERCEN_WW), 2, '.', ',').'</font></th>';
                echo '</tr>';
            } else {
                echo '<tr>';
                echo '<th><a href="ww/index.php?name=WW">วิธี WW</a></th>
                        <td align="center">'.number_format(round($wwOc), 2, '.', ',').'</td>
                        <td align="center">'.number_format(round($wwHc), 2, '.', ',').'</td>
                        <td align="center">'.number_format(round($wwOc+$wwHc), 2, '.', ',').'</td>
                        <th>'.number_format(round($SUM_WW), 2, '.', ',').'</th>
                        <th>'.number_format(round($PERCEN_WW), 2, '.', ',').'</th>';
                echo '</tr>';
            }
        }

        echo '</table>';
    ?>

        <br><br>
        <br><br>
    <center>
    <img src="img/Transformer.jpg" height="120">
    <img src="img/VoltageTransformer.jpg" height="120">
    <img src="img/FuseLink.png" height="120">
    <img src="img/FuseCutout.jpg" height="120">
    <img src="img/LowTension.jpg" height="120">
    <img src="img/StreetLighting20W.jpg" height="120">
    <img src="img/CurrentTransformer.jpg" height="120">

</body>
</html>
