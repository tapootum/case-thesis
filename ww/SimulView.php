<html>
  <head>
    <style type="text/css">
      body {
        padding: 10px;
        font: 14px;
        float: center;
      }

      .simul-table {
        border-collapse: collapse;
      }

      .simul-title-column {
        text-align: center;
        background-color: #5C9DED;
        border: 1px solid black;
        padding: 10px;
      }

      .simul-column {
        border: 1px solid black;
        padding: 10px;
      }

      .simul-amount-column {
        border: 1px solid black;
        padding: 10px;
        text-align: right;
      }

      .highlight {
        color: red;
      }

      .row-simul-summary {
        clear: both;
        width: 1350px;
        padding-top: 30px;
        float: center;
      }

      .row-simul-left {
        float: left;
      }
      .row-simul-center {
        float: center;
      }

      .row-simul-right {
        float: right;
      }
    </style>
  </head>

  <body>

    <div class="row-simul-summary">
      <div class="row-simul-center">
        <table class="simul-table">
          <tr>
            <td class="simul-title-column"><font color="#FFFFFF">ช่วงเวลา</td>
            <?php
              for($month = 1; $month <= $GLOBALS['MONTH']; $month++) {
                echo '<td class="simul-title-column"><font color="#FFFFFF">' . $month . '</td>';
              }
            ?>
            <td class="simul-title-column"bgcolor="#5C9DED"><font color="#FFFFFF">รวม</td>
          </tr>

          <tr>
            <td class="simul-column" bgcolor="#5C9DED"><font color="#FFFFFF">ความต้องการ</td>
            <?php
              foreach ($simulInput->getDemands() as $demand) {
                echo '<td class="simul-amount-column">' . number_format($demand) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulInput->calcSummaryDemand())?></td>
          </tr>

          <tr>
            <td class="simul-column" bgcolor="#5C9DED"><font color="#FFFFFF">ปริมาณการสั่ง (Q)</td>
            <?php
              foreach ($simulSummarys->getQtys() as $qty) {
                echo '<td class="simul-amount-column">' . number_format($qty) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcSummaryQty())?></td>
          </tr>

          <tr>
            <td class="simul-column" bgcolor="#5C9DED"><font color="#FFFFFF">สินค้าคงคลังต้นงวด</td>
            <?php
              foreach ($simulSummarys->getBegins() as $begin) {
                echo '<td class="simul-amount-column">' . number_format($begin) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcSummaryBegin())?></td>
          </tr>

          <tr>
            <td class="simul-column" bgcolor="#5C9DED"><font color="#FFFFFF">สินค้าคงคลังปลายงวด</td>
            <?php
              foreach ($simulSummarys->getEnds() as $end) {
                echo '<td class="simul-amount-column">' . number_format($end) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcSummaryEnd())?></td>
          </tr>

          <tr>
            <td class="simul-column" bgcolor="#5C9DED"><font color="#FFFFFF">สินค้าคงคลังถัวเฉลี่ย</td>
            <?php
              foreach ($simulSummarys->getAvgs() as $avg) {
                echo '<td class="simul-amount-column">' . number_format($avg) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcSummaryAvg())?></td>
          </tr>

          <tr>
            <td class="simul-column" bgcolor="#5C9DED"><font color="#FFFFFF">ค่าใช้จ่ายในการถือครอง</td>
            <?php
              foreach ($simulSummarys->getHoldingCosts() as $holdingCost) {
                echo '<td class="simul-amount-column">' . number_format($holdingCost, 2) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcHoldingCost(), 2)?></td>
          </tr>

          <tr>
            <td class="simul-column" bgcolor="#5C9DED"><font color="#FFFFFF">ค่าใช้จ่ายในการสั่ง (คงที่)</td>
            <?php
              foreach ($simulSummarys->getOrderCosts() as $orderingCost) {
                echo '<td class="simul-amount-column">' . number_format($orderingCost, 2) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcOrderingCost(), 2)?></td>
          </tr>

          <tr>
            <td class="simul-column" colspan="13" align="right"><b>ต้นทุนสินค้าคงคลังโดยรวม (บาท)</td>
            <td class="simul-amount-column"><b><?=number_format($simulSummarys->calcTc(), 2)?></td>
          </tr>
        </table>
      </div>
    </div>
  </body>
</html>
