<?php
  class SimulSummary {
    private $qtys = [];
    private $begins = [];
    private $ends = [];
    private $avgs = [];
    private $holdingCosts = [];
    private $orderCosts = [];

    public function __construct($qtys, $inventories, $avg, $holdingCosts, $orderCosts) {
      $this->qtys = $qtys;
      $this->begins = $inventories[0];
      $this->ends = $inventories[1];
      $this->avgs = $avg;
      $this->holdingCosts = $holdingCosts;
      $this->orderCosts = $orderCosts;
    }

    public function getQtys() {
      return $this->qtys;
    }

    public function getBegins() {
      return $this->begins;
    }

    public function getEnds() {
      return $this->ends;
    }

    public function getAvgs() {
      return $this->avgs;
    }

    public function getHoldingCosts() {
      return $this->holdingCosts;
    }

    public function getOrderCosts() {
      return $this->orderCosts;
    }

    public function calcSummaryQty() {
      return array_sum($this->qtys);
    }

    public function calcSummaryBegin() {
      return array_sum($this->begins);
    }

    public function calcSummaryEnd() {
      return array_sum($this->ends);
    }

    public function calcSummaryAvg() {
      return array_sum($this->avgs);
    }

    public function calcHoldingCost() {
      return array_sum($this->holdingCosts);
    }

    public function calcOrderingCost() {
      return array_sum($this->orderCosts);
    }

    public function calcTc() {
      return round($this->calcHoldingCost() + $this->calcOrderingCost(), 2);
    }
  }
