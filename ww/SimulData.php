<?php
  require './Const.php';

  class SimulData {
    private $simuls;

    public function __construct() {
      for($i = 0; $i < $GLOBALS['MONTH']; $i++) {
        $this->simuls[$i] = '';
      }
    }

    public function getSimuls() {
      return $this->simuls;
    }

    public function addSimulByMonth($month, $simulData) {
      $this->simuls[$month] = $simulData;
    }
  }