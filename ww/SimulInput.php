<?php
  require './Const.php';

  class SimulInput {
    private $demands;
    private $s;
    private $h;
    private $c;
    private $H;
    private $dBar;

    public function __construct($demands, $s, $h, $c) {
      $this->demands = $demands;
      $this->s = $s;
      $this->h = $h;
      $this->c = $c;
      $this->H = $this->calcHValue();
      $this->dBar = $this->calcDBarValue();
    }

    public function getDemands() {
      return $this->demands;
    }

    public function getS() {
      return $this->s;
    }

    public function getH() {
      return $this->h;
    }

    public function getC() {
      return $this->c;
    }

    public function getHCalc() {
      return $this->H;
    }

    public function getdBar() {
      return $this->dBar;
    }

    private function calcHValue() {
      return $this->c * (0.2 / 12);
    }

    private function calcDBarValue() {
      return array_sum($this->demands) / $GLOBALS['MONTH'];
    }

    public function calcSummaryDemand() {
      return array_sum($this->demands);
    }
  }
