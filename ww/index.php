<!DOCTYPE html>
<html lang="en">

<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="shortcut icon" href="../img/favicon.png" />
    <link rel="stylesheet" href="../build/css-mint.min.css">
    <style>
    .grid *[class*='col-span'] > div {
        color: #fff;
        background-color: #5C9DED;
        margin: 5px;
        padding: 7px;
        font-weight: 300;
    }
    </style>
</head>

<body style="padding: 30px 30px 50px;">
    <header class="header">
        <div class="logo">
            <a href="index.php"><h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1></a>
        </div>
        <div class="nav-right">
            <nav class="navbar">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </nav>
        </div>
    </header><br><br>
    <br><br>
    <ul class="breadcrumb">
        <li><a href="../index.php">หน้าแรก</a></li>
        <li><a href="../excel-upload.php">เลือกวิธีการสั่งซื้อ</a></li>
        <li><a href="../menucal.php">เปรียบเทียบวิธีการสั่งซื้อ</a></li>
        <li><a href="#"><?php echo $_GET['name']; ?></a></li>
    </ul>
        <br><br>
<?php
  session_start();
  $product_name = $_SESSION["WW_DATA"][0];
  $rowData = $_SESSION["WW_DATA"][1];
  require './SimulInput.php';
  require './SimulCalculate.php';
  require './SimulSummaryCalculate.php';
  error_reporting(0);
  include '../db/db_conn.php';
  $sql = "SELECT * FROM product";
  $resultx = $conn->query($sql);
  if ($resultx->num_rows > 0) {
    // output data of each row
      while($rowO=$resultx->fetch_assoc()) {
        if ($rowO["PRODUCT_NAME"] == $product_name){
              $S = $rowO["S"];
              $h = $rowO["H"];
              $C = $rowO["C"];
              $MIN = $rowO["MIN"];
            // echo $S.$h.$C.'<br>';
          }
      }
  }
   else {
      echo "0 results";
      exit;
  }
  $simulInput = new SimulInput([$rowData[0],$rowData[1],$rowData[2],$rowData[3],$rowData[4],$rowData[5],$rowData[6],$rowData[7],$rowData[8],$rowData[9],$rowData[10],$rowData[11]],
  $S,
  $h,
  $C);

  $simulCalc = new SimulCalculate($simulInput);
  $simulVal = $simulCalc->calcSimul();

  $simulDetails = $simulVal[0];
  $simulMins = $simulVal[1];

  $simulSummaryCalc = new SimulSummaryCalculate($simulCalc);
  $simulSummarys = $simulSummaryCalc->calcSummary($simulInput, $simulMins);



  
require('./SimulView.php');

echo '
	<br><br>
        <br><br>
        <br><br>
        <br><br>
        <br><br>
        <br><br>
        <br><br>
        <br><br>
        <br><br>
        <br><br>
        <br><br>
        <br><br>
    <center>
    <img src="../img/Transformer.jpg" height="120">
    <img src="../img/VoltageTransformer.jpg" height="120">
    <img src="../img/FuseLink.png" height="120">
    <img src="../img/FuseCutout.jpg" height="120">
    <img src="../img/LowTension.jpg" height="120">
    <img src="../img/StreetLighting20W.jpg" height="120">
    <img src="../img/CurrentTransformer.jpg" height="120">
';
