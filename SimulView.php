<html>
  <head>
    <style type="text/css">
      body {
        padding: 10px;
        font: 14px;
      }

      .simul-table {
        border-collapse: collapse;
      }

      .simul-title-column {
        text-align: center;
        background-color: grey;
        border: 1px solid black;
        padding: 10px;
      }

      .simul-column {
        border: 1px solid black;
        padding: 10px;
      }

      .simul-amount-column {
        border: 1px solid black;
        padding: 10px;
        text-align: right;
      }

      .highlight {
        color: red;
      }

      .row-simul-summary {
        clear: both;
        width: 1350px;
        padding-top: 30px;
      }

      .row-simul-left {
        float: left;
      }

      .row-simul-right {
        float: right;
      }
    </style>
  </head>

  <body>
    <table class="simul-table">
      <?php
      foreach ($simulDetails as $simulDetail) {
        echo '<tr>';
        foreach ($simulDetail->getSimuls() as $month => $simulDetailMonth) {
          if($simulDetailMonth == $simulMins[$month]->getSimul()) {
            echo '<td class="simul-amount-column highlight">';
          } else {
            echo '<td class="simul-amount-column">';
          }

          echo gettype($simulDetailMonth) == 'double'? number_format($simulDetailMonth): '';
          echo '</td>';
        }
        echo '</tr>';
      }
      ?>
    </table>

    <div class="row-simul-summary">
      <div class="row-simul-left">
        <table class="simul-table">
          <tr>
            <td class="simul-title-column"></td>
            <td class="simul-title-column">ช่วงเวลา</td>
            <?php
              for($month = 1; $month <= $GLOBALS['MONTH']; $month++) {
                echo '<td class="simul-title-column">' . $month . '</td>';
              }
            ?>
            <td class="simul-title-column">รวม</td>
          </tr>

          <tr>
            <td class="simul-column">Demand</td>
            <td class="simul-column">ความต้องการ</td>
            <?php
              foreach ($simulInput->getDemands() as $demand) {
                echo '<td class="simul-amount-column">' . number_format($demand) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulInput->calcSummaryDemand())?></td>
          </tr>

          <tr>
            <td class="simul-column">Quantity</td>
            <td class="simul-column">ปริมาณการสั่ง (Q)</td>
            <?php
              foreach ($simulSummarys->getQtys() as $qty) {
                echo '<td class="simul-amount-column">' . number_format($qty) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcSummaryQty())?></td>
          </tr>

          <tr>
            <td class="simul-column">Beginning Inventory</td>
            <td class="simul-column">สินค้าคงคลังต้นงวด</td>
            <?php
              foreach ($simulSummarys->getBegins() as $begin) {
                echo '<td class="simul-amount-column">' . number_format($begin) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcSummaryBegin())?></td>
          </tr>

          <tr>
            <td class="simul-column">Ending Inventory</td>
            <td class="simul-column">สินค้าคงคลังปลายงวด</td>
            <?php
              foreach ($simulSummarys->getEnds() as $end) {
                echo '<td class="simul-amount-column">' . number_format($end) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcSummaryEnd())?></td>
          </tr>

          <tr>
            <td class="simul-column">Average Inventory</td>
            <td class="simul-column">สินค้าคงคลังถัวเฉลี่ย</td>
            <?php
              foreach ($simulSummarys->getAvgs() as $avg) {
                echo '<td class="simul-amount-column">' . number_format($avg) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcSummaryAvg())?></td>
          </tr>

          <tr>
            <td class="simul-column">Holding Cost</td>
            <td class="simul-column">ค่าใช้จ่ายในการถือครอง</td>
            <?php
              foreach ($simulSummarys->getHoldingCosts() as $holdingCost) {
                echo '<td class="simul-amount-column">' . number_format($holdingCost) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcHoldingCost())?></td>
          </tr>

          <tr>
            <td class="simul-column">Ordering Cost</td>
            <td class="simul-column">ค่าใช้จ่ายในการสั่ง (คงที่)</td>
            <?php
              foreach ($simulSummarys->getOrderCosts() as $orderingCost) {
                echo '<td class="simul-amount-column">' . number_format($orderingCost) . '</td>';
              }
            ?>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcOrderingCost())?></td>
          </tr>

          <tr>
            <td class="simul-column" colspan="13"></td>
            <td class="simul-column">TC</td>
            <td class="simul-amount-column"><?=number_format($simulSummarys->calcTc())?></td>
          </tr>
        </table>
      </div>

      <div class="row-simul-right">
        <table class="simul-table">
          <tr>
          </tr>
          <tr>
            <td class="simul-column">d-bar</td>
            <td class="simul-amount-column"><?=number_format($simulInput->getDBar())?></td>
          </tr>
          <tr>
            <td class="simul-column">S</td>
            <td class="simul-amount-column"><?=number_format($simulInput->getS())?></td>
          </tr>
          <tr>
            <td class="simul-column">h</td>
            <td class="simul-amount-column"><?=number_format($simulInput->getH())?></td>
          </tr>
          <tr>
            <td class="simul-column">c</td>
            <td class="simul-amount-column"><?=number_format($simulInput->getC())?></td>
          </tr>
          <tr>
            <td class="simul-column">H</td>
            <td class="simul-amount-column"><?=number_format(round($simulInput->getHCalc(), 2))?></td>
          </tr>
        </table>
      </div>
    </div>
  </body>
</html>