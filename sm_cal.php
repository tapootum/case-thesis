<?php

function Main_Cal($MainTable, $CalTable, $dbar){
		$Month = 0;
		$KM_Current = 1;
		$KM_Next = 0;
		$Step = 0;
		$Period = 0;
		$rr=0;
		$GG = 0;
		do{
			do{
				//Cal KM Current Month
				$KM_Current = Cal_KM($CalTable,$Month,$Month+$Step);
				$Step++;
				//Change Quantity on CurrentMonth and NextMonth 
				$CalTable[1][$Month] = $CalTable[1][$Month] + $CalTable[1][$Month+$Step];
				$CalTable[1][$Month+$Step] = 0;
				// //Re Cal CalTable
				$CalTable = Refresh($CalTable, $Month, $Month+$Step, $dbar);

				$KM_Next = Cal_KM($CalTable, $Month, $Month+$Step);

				if($KM_Current > $KM_Next){
					$Period++;
				} 

			}while(($KM_Current>$KM_Next) && (($Month+$Step)<11));
			if($Period > 0){
				$CC = Copy_Table($CalTable,$MainTable,$Month,$Month+$Period);
				$MainTable = $CC["MainT"];
				$CalTable = $CC["CalT"];
			}else{
				$CC = Copy_Table($MainTable,$CalTable,$Month,$Month+$Step);
				$MainTable = $CC["MainT"];
				$CalTable = $CC["CalT"];
			}
			
			$Period = 0;
			if ($Step == 0){
				$Month += 1;
			} else {
				$Month += $Step;
			}
			$Step = 0;
		    $rr++;
		    if($rr==12){
				exit();
			}
		}while($Month<11);
		$SM_DATA = ShowResult($MainTable);
		return $SM_DATA;
}
	
function Cal_KM($table, $start, $stop){
		$sum=0;
		for($i=$start; $i<=$stop; $i++){
			$sum = $sum + ($table[5][$i]+$table[6][$i])/($stop-$start+1);
		}
		return $sum;
	}
	
function ShowResult($table){
	$Total = 0;
	$Total_HC = 0;
	$Total_OC = 0;
		for($i=0; $i<=6; $i++){
			for($j=0; $j<=11; $j++){
				if($i==5 || $i==6){
					$Total += $table[$i][$j];
				}
				if($i==5){
					$Total_HC += $table[$i][$j];
				}
				if($i==6){
					$Total_OC += $table[$i][$j];
				}
			}
		}
	$data = array(2 => $Total_HC, 3 => $Total_OC, 1 => $Total, 4 => $table);
	return $data;
	}
	
function Refresh($table, $start, $stop, $dbar){
		for($i=$start; $i<=$stop; $i++){
			if($i==0){
				$table[2][$i] = $table[1][$i];
				$table[3][$i] = $table[2][$i] - $table[0][$i];
				$table[4][$i] = ($table[3][$i]+$table[2][$i])/2;
				$table[5][$i] = $table[4][$i]*($dbar[2]/12)*$dbar[3];
				if($table[1][$i]>0){
					$table[6][$i] = $dbar[1];
				}else{
					$table[6][$i] = 0;
				}
			}else{
				$table[2][$i] = $table[3][$i-1]+$table[1][$i];
				$table[3][$i] = $table[2][$i]-$table[0][$i];
				$table[4][$i] = ($table[3][$i]+$table[2][$i])/2;
				$table[5][$i] = $table[4][$i]*($dbar[2]/12)*$dbar[3];
				if($table[1][$i]>0){
					$table[6][$i] = $dbar[1];
				}else{
					$table[6][$i] = 0;
				}
				
			}
		}
		return $table;
	}
	
function Copy_Table($MainTable, $CalTable, $start, $n){
		for($i=0; $i<=6; $i++){
			for($j=$start; $j<=$n; $j++){
				$CalTable[$i][$j] = $MainTable[$i][$j];
			}
		}
	 $value = array("MainT" =>$MainTable, "CalT" => $CalTable );
	 return $value;
	}
	
function SM($product_name, $rawDATA){
		global $dbar;
		$CalTable[0][0] = "";
		$MainTable = InitialMainTable($rawDATA);
		$dbar = Initial_dbar($MainTable,$product_name);
		$MainTable = Refresh($MainTable,0,11,$dbar);
		$CC = Copy_Table($MainTable, $CalTable, 0, 11);
		$MainTable = $CC["MainT"];
		$CalTable = $CC["CalT"];
		$data = Main_Cal($MainTable, $CalTable,$dbar);
		return $data;
	}
	
function Initial_dbar($MainTable,$product_name){
	include 'db/db_conn.php';
	$sql = "SELECT * FROM product";
	$resultx = $conn->query($sql);
	if ($resultx->num_rows > 0) {
	    // output data of each row
	    while($rowO=$resultx->fetch_assoc()) {
	    	if ($rowO["PRODUCT_NAME"] == $product_name){
	            $S = $rowO["S"];
	            $h = $rowO["H"];
	            $C = $rowO["C"];
	            $MIN = $rowO["MIN"];
	        }
	    }
	}
	 else {
	    echo "0 results";
	    exit;
	}
	    $dbarx[0] = 0;
		for($i=0;$i<12;$i++){
			$dbarx[0]+=$MainTable[0][$i];
		}
		$dbarx[0] = $dbarx[0]/12;
		$dbarx[1] = (148000.00/3/86);
		$dbarx[2] = 0.20;
		$dbarx[3] = $C;
		$dbarx[4] = round($dbarx[3]*(0.2/12),2);
		return $dbarx;
	}
	
function InitialMainTable($rawDATA){
		for($i=0; $i<=1; $i++){
			for($j=0; $j<12; $j++){
				$MainTable[$i][$j] = $rawDATA[$j];
			}
		}
		$MainTable[1][0] = $MainTable[0][0];
		return $MainTable;
	}
?>
