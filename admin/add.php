<?php
session_start();
include '../db/db_conn.php';
if (!$_SESSION["UserID"]){  //check session

	  Header("Location: index.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form

}else{?>
<!doctype html>
<html>
<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="shortcut icon" href="../img/favicon.png" />
    <link rel="stylesheet" href="../build/css-mint.css">
    <style>
    .grid *[class*='col-span'] > div {
        color: #fff;
        background-color: #5C9DED;
        margin: 5px;
        padding: 7px;
        font-weight: 100%;
    }
    </style>
</head>

<body style="padding: 30px 30px 100%;">
    <header class="header">
        <div class="logo">
            <a href="../index.php"><h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1></a>
        </div>
        <div class="nav-right">
            <nav class="navbar">
                <ul>
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="../about.php">About</a></li>
                    <li><a href="../contact.php">Contact</a></li>
                    <li><a href="admin.php">Admin</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </nav>
        </div>
    </header><br>
<form action="new.php" name="frmAdd" method="post">
<table>
    <tr>
        <td><center>ผลิตภัณฑ์</td>
        <td><center>ค่าใช้จ่ายในการสั่งสินค้า<br>(S) (บาท/ครั้ง)</td>
        <td><center>ต้นทุนสินค้าต่อหน่วย<br>(C) (บาท/หน่วย)</td>
        <td><center>ค่าใช้จ่ายในการถือครองสินค้า<br>(H) (บาท/หน่วย)</td>
        <td><center>ระดับสินค้าคงคลังปลอดภัย<br>(หน่วย)</td>
        <td> </td>
    </tr>
    </tr>
    <tr>
        <td width="45%"><input type="text" name="productName" size="20"></td>
        <td width="11%"><input type="text" name="S" size="5"></td>
        <td width="11%"><input type="text" name="H" size="5"></td>
        <td width="11%"><input type="text" name="C" size="5"></td>
        <td width="11%"><input type="text" name="MIN" size="5"></td>
        <td> </td>
    <tr>
        <td colspan=6><input type="submit" name="submit" value="submit"></td>
    </tr>
</table>
</form>

</body>
</html>
<?php }?>

