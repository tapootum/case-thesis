<?php
session_start();
include '../db/db_conn.php';
if (!$_SESSION["UserID"]){  //check session

	  Header("Location: index.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form

}else{
        $NUMBER=0;
?>
<!doctype html>
<html>
<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="shortcut icon" href="../img/favicon.png" />
    <link rel="stylesheet" href="../build/css-mint.css">
    <style>
    .grid *[class*='col-span'] > div {
        color: #fff;
        background-color: #5C9DED;
        margin: 5px;
        padding: 7px;
        font-weight: 100%;
    }
    #tdw {
    	width: 200px;
    }
    #tds {
    	width: 500px;
    }
    #tdr {
    	width: 200px;
    }
    </style>
</head>

<body style="padding: 30px 30px 100%;">
    <header class="header">
        <div class="logo">
            <a href="../index.php"><h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1></a>
        </div>
        <div class="nav-right">
            <nav class="navbar">
                <ul>
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="../about.php">About</a></li>
                    <li><a href="../contact.php">Contact</a></li>
                    <li><a href="admin.php">Admin</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </nav>
        </div>
    </header><br>
<table>
    <tr>
        <th><center>ลำดับ</th>
        <th><center>ผลิตภัณฑ์</th>
        <th><center>ค่าใช้จ่ายในการสั่งสินค้า<br>(S) (บาท/ครั้ง)</th>
        <th><center>ต้นทุนสินค้าต่อหน่วย<br>(C) (บาท/หน่วย)</th>
        <th><center>ค่าใช้จ่ายในการถือครองสินค้า<br>(H) (บาท/หน่วย)</th>
        <th><center>ระดับสินค้าคงคลังปลอดภัย<br>(หน่วย)</th>
        <th> </th>
        <th> </th>
        <th> </th>
    </tr>
	<?php
        $sql = "SELECT * FROM product";
	$resultx = $conn->query($sql);
        if ($resultx->num_rows > 0) {
	    // output data of each row
	    while($rowO=$resultx->fetch_assoc()) {
            $NUMBER = $NUMBER + 1;
            echo '<tr>';    
            echo '<td><center>'.$NUMBER.'</td>';       
            echo '<td id="tds">'.$rowO["PRODUCT_NAME"].'</td>';       
            echo '<td id="tdr" align="right">'.number_format($rowO["S"],2,".",",").'</td>';       
            echo '<td id="tdr" align="right">'.number_format($rowO["C"],2,".",",").'</td>';       
            echo '<td id="tdw" align="right">'.number_format($rowO["H"],2,".",",").'</td>';       
            echo '<td id="tdw"><center>'.$rowO["MIN"].'</td>';       
            echo '<td><a href="editRecord.php?ID='.$rowO["ID"].'">Edit</a></td>';
            echo '<td>     </td>';
            ?>
            <td width="10%" align="right"><a href="JavaScript:if(confirm('Confirm Delete?') == true){window.location='delRecord.php?ID=<?php echo $rowO["ID"];?>';}">Delete</a></td>
	    <?php
            echo '</tr>';
	    }
        }
        else {
            echo "0 results";
            exit;
        } 
        ?>
    <tr><td colspan=9><br></td></tr>
    <tr><td></td>
        <td colspan=8><a href="add.php"><form>
<input type="button" value="เพิ่มผลิตภัณฑ์ใหม่" onclick="window.location.href='add.php'" />
</form></td>
    </tr>
</table>


</body>
</html>
<?php }?>

