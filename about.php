<!DOCTYPE html>
<html lang="en">

<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="shortcut icon" href="img/favicon.png" />
    <link rel="stylesheet" href="build/css-mint.min.css">
    <style>
    .grid *[class*='col-span'] > div {
        color: #fff;
        background-color: #5C9DED;
        margin: 5px;
        padding: 7px;
        font-weight: 100%;
    }
    </style>
</head>

<body style="padding: 30px 30px 100%;">
    <header class="header">
        <div class="logo">
            <a href="index.php"><h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1></a>
        </div>
        <div class="nav-right">
            <nav class="navbar">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="admin/admin.php">Admin</a></li>
                </ul>
            </nav>
        </div>
    </header>
         <br><br>
         <br><br>
     <ul class="breadcrumb">
       <li><a href="index.php">หน้าแรก</a></li>
       <li><a href="#">About</a></li>
     </ul>
    <br><br>
    <br><br>
<h4><strong>About</strong></h4>
<tbody>
<tr>
<td>
&nbsp; &nbsp; &nbsp;ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม เป็นระบบที่ถูกพัฒนาขึ้น เพื่อช่วยให้หน่วยงานธุรกิจซื้อมาขายไป หรือผู้ใช้งานอื่น ตัดสินใจเลือกวิธีการสั่งซื้อสินค้าที่เหมาะสมโดยจะพิจารณาจากต้นทุนการจัดการสินค้าคงคลังโดยรวม (Total Cost = Holding Cost + Inventory Cost) ที่ต่ำที่สุด เนื่องจากระบบนี้ เป็นระบบที่ให้ผู้ใช้งาน ตัดสินใจเลือกวิธีการสั่งซื้อของสินค้าที่จะเกิดขึ้นในอนาคต ดังนั้นผู้ใช้งานจะต้องมีข้อมูลพยากรณ์ของสินค้าที่ผู้ใช้งานต้องการเลือกวิธีการสั่งซื้อด้วย เพื่อเป็นข้อมูลเริ่มต้นสำหรับการใช้งานระบบนี้ ทั้งนี้ ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม เป็นระบบที่ถูกพัฒนาขึ้นให้ทำงานบนระบบเครือข่ายได้</h3>
</td>
</tr>
</tbody>



    <center>
    <br>
    <br>
    <br>
    <br>
    <img src="img/Transformer.jpg" height="120">
    <img src="img/VoltageTransformer.jpg" height="120">
    <img src="img/FuseLink.png" height="120">
    <img src="img/FuseCutout.jpg" height="120">
    <img src="img/LowTension.jpg" height="120">
    <img src="img/StreetLighting20W.jpg" height="120">
    <img src="img/CurrentTransformer.jpg" height="120">
</body>

</html>
