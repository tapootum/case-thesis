<?php

/** Set default timezone (will throw a notice otherwise) */
date_default_timezone_set('Asia/Kolkata');

include 'PHPExcel/IOFactory.php';

function dbar($d_bar_y){
	return $d_bar_y/12;
}
function est_var_d($d_bar_x, $evd_x){
	$dbar_s = dbar($d_bar_x);
	$estdvar = ($evd_x/12) - ($dbar_s*$dbar_s);
	return $estdvar;
}
function vc($d_bar_z, $evd_z){
	$dbar_a = dbar($d_bar_z);
	$estdvar = est_var_d($d_bar_z, $evd_z);
	$vc_s = $estdvar/($dbar_a*$dbar_a);
	return $vc_s;
}

if(isset($_FILES['file']['name'])){
		
	$file_name = $_FILES['file']['name'];
	$ext = pathinfo($file_name, PATHINFO_EXTENSION);
	
	//Checking the file extension
	if($ext == "xlsx"){
			
			$file_name = $_FILES['file']['tmp_name'];
			$inputFileName = $file_name;

		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}

		//Table used to display the contents of the file
		#echo '<center><table style="width:50%;" border=1>';
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		//  Loop through each row of the worksheet in turn
		$sum_data1 = 0;
		$sum_data2 = 0;
		$xx = 0;
		for ($row = 2; $row <= $highestRow; $row++) {
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
			#echo "<tr>";
			//echoing every cell in the selected row for simplicity. You can save the data in database too.
			foreach($rowData[0] as $k=>$v){
				$sum_data1 = $sum_data1+$v;
				$sum_data2 = $sum_data2+($v*$v);
				#echo "<td>".$v."</td>";
			}
			#echo "</tr>";
		}
		
		$dbar_x = dbar($sum_data1);
		$estvard = est_var_d($sum_data1, $sum_data2);
		$vcx = vc($sum_data1, $sum_data2);
		echo "D Bar : " . $dbar_x . '<br>';
		echo "est var d : " . $estvard . '<br>';
		echo "VC : " . $vcx . '<br>';
		#echo '</table></center>';
		
	}

	else{
		echo '<p style="color:red;">Please upload file with xlsx extension only</p>'; 
	}	
		
}

?>